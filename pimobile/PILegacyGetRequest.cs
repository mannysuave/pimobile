﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace PIMobile
{
    public class PILegacyGetRequest
    {
        public virtual string Host { get; protected set; }

        protected virtual HttpWebRequest Request { get; set; }
        protected virtual Uri Url { get; set; }
        protected virtual string Domain { get; set; }
        protected virtual Dictionary<string, string> Cookies { get; set; }

        public PILegacyGetRequest(string domain)
        {
            this.Domain = domain;
            this.CreateUri(domain);            
            this.Host = this.Url.Host;
            this.Cookies = new Dictionary<string, string>();
        }

        public virtual void AddCookie(string name, string value)
        {
            this.Cookies.Add(name, value);
        }

        public virtual string GetAuthorizedString()
        {
            var response = this.GetResponse();
            var responseString = this.GetResponseString(response);
            return responseString;
        }

        public virtual string GetUnauthorizedString()
        {
            var response = this.GetUnauthorizedResponse();
            if (!this.IsResponseUnauthorized(response))
            {
                throw new InvalidOperationException("Request was not Unauthorized");
            }
            return this.GetResponseString(response);
        }

        public virtual void AddQueryParam(string name, string value)
        {
            var newParam = string.Format("{0}={1}&", name, value);
            var query = this.Url.Query;
            if (query == string.Empty)
            {
                query = "?";
            }
            query = query + newParam;
            this.CreateUri(this.Domain, query);
        }

        protected virtual void CreateUri(string domain, string query = null)
        {
            var format = "http://{0}/services";
            var url = string.Format(format, domain);
            if (query != null)
            {
                url = url + query;
            }
            this.Url = new Uri(url);
        }

        protected virtual HttpWebResponse GetUnauthorizedResponse()
        {
            HttpWebResponse response = null;
            try
            {
                var resp = (HttpWebResponse)this.GetResponse();
            }
            catch (WebException ex)
            {
                response = (HttpWebResponse)ex.Response;
            }
            return response;
        }

        protected virtual HttpWebResponse GetResponse()
        {
            if (this.Request == null)
            {
                this.CreateRequest();
            }
            var response = (HttpWebResponse)this.Request.GetResponse();
            return response;
        }

        protected virtual bool IsResponseUnauthorized(HttpWebResponse response)
        {
            return response != null && response.StatusCode == HttpStatusCode.Unauthorized;
        }

        protected virtual string GetResponseString(HttpWebResponse response)
        {
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            response.Close();
            return responseString;
        }

        protected virtual void CreateRequest()
        {
            this.Request = (HttpWebRequest)WebRequest.Create(Url);
            this.Request.CookieContainer = this.GetCookieContainer();
            this.AddDefaultHeaders();
        }

        protected virtual CookieContainer GetCookieContainer()
        {
            var realCookies = this.Cookies.Keys.Select(x => new Cookie(x,this.Cookies[x],this.Url.LocalPath, this.Host)).ToList();
            var container = new CookieContainer();
            realCookies.ForEach(x => container.Add(x));
            return container;
        }

        protected virtual void AddDefaultHeaders()
        {
            this.Request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            this.Request.Headers.Add("Accept-Encoding", "gzip, deflate");
            this.Request.Headers.Add("Accept-Language", "en-US,en;q=0.8,el;q=0.6,he;q=0.4");
            this.Request.Headers.Add("Cache-Control", "max-age=0");
            this.Request.Connection = "keep - alive";
            this.Request.ContentType = "application / x - www - form - urlencoded";
            this.Request.Host = this.Url.Authority;
            this.Request.Headers.Add("Origin", "http://" + this.Domain);
            this.Request.Referer = this.Url.OriginalString;
            this.Request.UserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36";
        }
    }
}