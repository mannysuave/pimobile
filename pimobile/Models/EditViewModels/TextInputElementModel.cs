﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PIMobile.Models.EditViewModels
{
    public class TextInputElementModel : InputElementModel
    {
        public virtual string Caption { get; protected set; }

        public TextInputElementModel(string caption, string name, string value)
            : base(name, value)
        {
            this.Caption = caption;
        }
    }
}
