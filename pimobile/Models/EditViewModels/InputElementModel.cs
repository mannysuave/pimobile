﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIMobile.Models.EditViewModels
{
 public class InputElementModel
    {
        public virtual string Name { get; protected set; }
        public virtual string Value { get; protected set; }

        public InputElementModel(string name, string value)
        {
            this.Name = name;
            this.Value = value;         
        }
    }
}