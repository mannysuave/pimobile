﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIMobile.Models.EditViewModels
{
    public class InputElements
    {
        public string Name { get; protected set; }
        public IEnumerable<InputElementModel> Elements { get; protected set; }

        public InputElements(string name, IEnumerable<InputElementModel> elements)
        {
            this.Name = name;
            this.Elements = elements;
        }
    }
}