﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PIMobile.Models
{
    public class StudyViewModel
    {
        public virtual string Id { get; set; }
        public virtual string Name { get; set; }

        public StudyViewModel() { }

        public StudyViewModel(string id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}
