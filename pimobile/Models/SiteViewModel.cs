﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIMobile.Models
{
    public class SiteViewModel
    {
        public virtual string StudyName { get; set; }
        public virtual string StudyId { get; set; }
        public virtual string Id { get; set; }
        public virtual string Name { get; set; }

        public SiteViewModel() { }

        public SiteViewModel(StudyViewModel study, string id, string name)
        {
            this.Id = id;
            this.Name = name;
            this.StudyName = study.Name;
            this.StudyId = study.Id;
        }
    }
}